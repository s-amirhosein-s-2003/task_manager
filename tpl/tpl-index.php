<!DOCTYPE html>
<html lang="en" >
  <head>
    <meta charset="UTF-8">
    <title><?= Site_Title ?></title>
    <link rel="stylesheet" href="<?= Base_URL ?>assets/css/style.css">
  </head>

  <body>
    <!-- partial:index.partial.html -->
    <div class="page">
      <div class="pageHeader">
        <div class="title">Dashboard</div>
        <div class="userPanel"><a href="<?=Base_URL.'?logout=1'?>"><i class="fa fa-sign-out"></i></a><span class="username"><?= $user->name ?? 'Unknown User' ?></span><img src="<?= Base_URL ?>assets/img/steve2.jpg" width="40" height="40"/></div>
      </div>
      <div class="main">
        <div class="nav">
          <div class="searchbox">
            <div><i class="fa fa-search"></i>
              <input type="search" placeholder="Search"/>
            </div>
          </div>
          <div class="menu">
            <div class="title">FOLDERS</div>
            <ul id="folder_list" class="folder_list">
              <li> 
                <a href="<?=Base_URL?>" class="<?=(!isset($_GET['folder_id'])) ?'active':'' ?>" >
                  <i class="fa fa-home"></i>Home
                </a>
              </li>
              
              <?php foreach($folders as $folder):?>
              <?=empty($folder)?'No task':''?>
              <a href="?folder_id=<?=$folder->id?>"               class="<?=($_GET['folder_id']==$folder->id)?'active':''  ?>">
                <li>
                  <i class="fa fa-folder"></i><?=$folder->name?>
                  <a href="?delete_folder=<?=$folder->id?>"           onclick="return confirm('Are you sure to delete this item ?');">
                    <i class="delete">x</i>
                  </a>
                </li>
              </a>
              <?php endforeach?>
            </ul>
          </div>

          <div class="addfolderbox" >
            <input type="input" id="addFolderInput" name="addFolderInput" placeholder="Add New Folder" />
            <button id="addFolderBtn" class="addFolderBtn">+</button>
          </div>

        </div>
        <div class="view">
          <div class="viewHeader">
            <div class="title">
              <input type="input" id="addTaskInput" class="addTaskInput" name="addTaskInput" placeholder="Add New Task">
            </div>
            <div class="functions">
              <div id="addTaskBtn"class="button active">Add New Task</div>
              <div class="button">Completed</div>
              <div class="button inverz"><i class="fa fa-trash-o"></i></div>
            </div>
          </div>
          <div class="content">
            <div class="list">
              <div class="title">Today</div>
              <ul>
                <!-- Manage Tasks -->                
                <?php if(sizeof($tasks)>0): ?>
                <?php foreach($tasks as $task):?>
                <li class="<?= $task->status ? 'checked':'' ?>">
                  <i data-taskId="<?= $task->id ?>" class="isDone <?= $task->status ? 'fa fa-check-square-o ':'fa fa-square-o ' ?>"></i>
                  <span><?=$task->title?></span>
                  <div class="info">
                    <span class="date">Created At : <?=$task->created_at?></span>
                    <a href="?delete_task=<?=$task->id?>"           onclick="return confirm('Are you sure to delete this item ?');">
                      <i class="delete">x</i>
                    </a>
                  </div>
                </li>
                <?php endforeach?>
                <?php else: ?>
                  <i>No Task Here ... </i>
                <?php endif;?>
              </ul>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- partial -->
    <script src="<?= Base_URL ?>assets/JS/jquery-3.7.1.min.js"></script>
    <script  src="<?= Base_URL ?>assets/JS/script.js"></script>

    <script>

      $(document).ready(function(){

        $("#addTaskInput").focus();

        function addFolderFunction(){
          var input = $("#addFolderInput");
          $.ajax({
            url:"process/ajaxHandler.php",
            type:"POST",
            data:{action:"addFolder",folderName:input.val()},
            success:function(response){
              if(response ==1){
                $('<li> <a href="#"> <i class="fa fa-folder"></i>'+input.val()+' </a> <a href="#"> <i class="delete">x</i> </a> </li>').appendTo("ul#folder_list");
                input.val('');
              }
            }
          });
          
        }

        function addTaskFunction(){
          var input = $("#addTaskInput");
          $.ajax({
            url:"process/ajaxHandler.php",
            type:"POST",
            data:{action:"addTask",folderId : <?= $_GET['folder_id'] ?? 0 ?> ,taskTitle : input.val()},      
            success:function(response){
              if(response==1){
                location.reload();
                input.val('');
              }else{
                alert(response);
              }
            }
            
          });
        }


        $("#addFolderBtn").click (function(event){
          addFolderFunction();
        });
        $("#addFolderInput").keypress(function(e){
          if(e.which == 13) {
            addFolderFunction();
          }
        });
        $("#addTaskInput").keypress(function(e){
          if(e.which == 13) {
            addTaskFunction()
          }    
        });
        $("#addTaskBtn").click(function(){
          addTaskFunction();
        });


        $(".isDone").click(function(){
          var tid=$(this).attr('data-taskId');
          $.ajax({
            url:"process/ajaxHandler.php",
            type:"POST",
            data:{action:'doneSwitch',taskId:tid},
            success:function(response){
              if(response == 1){
                location.reload();
              }else{
                alert(response);
              }
            }

          });
        });


        /*
        $("").click(
            function(){
                swal("Good job!", "تو موفق شدی پسر", "success");

            }
        );

        $("").click(
            function(){
                swal("Oh No", "مشکلی به وجود اومد", "error");
            }
        );
        */

      });
      
    </script>
  </body>
</html>
