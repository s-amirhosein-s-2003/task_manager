<?php


include "bootstrap/init.php";

if(isset($_GET['logout'])){
    logOut() ;
}


if(!isLoggedIn()){
    //header("location: https://7learn.com/ ");

    header("location: ".Base_URL."auth.php ");
}
$user = getLoggedInUser();
//اگر delete_folder در url ست شد و مقدارش عددی بود
if(isset($_GET['delete_folder']) && is_numeric($_GET['delete_folder'])){
    deleteFolders($_GET['delete_folder']);
}

//Delete Tasks
if(isset($_GET['delete_task']) && is_numeric($_GET['delete_task'])){
    deleteTasks($_GET['delete_task']);
}


if(isset($_GET['addFolder']) ){
    addFolders($_GET['addFolder']);
}

$folders=getFolders();

$tasks=getTasks();


#قالب اصلی
include 'tpl/tpl-index.php';