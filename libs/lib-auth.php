<?php


/**logIn**/
function getUserByEmail($email){
    global $pdo;
    $sql="SELECT * FROM user WHERE email = :email ;";
    $stmt=$pdo->prepare($sql);
    $stmt->execute(array(':email'=>$email));
    $records=$stmt->fetchAll(PDO::FETCH_OBJ);
    return $records[0] ?? null;
}

function login($email,$password){
    $user=getUserByEmail($email);
    if( is_null($user) ){
        return false;
    }
    if(password_verify($password,$user->password)){
        #Login is SUCCESSFULL
        $_SESSION['login']=$user;
        return true;
    }
    return false;
}


function isLoggedIn(){
    return (isset($_SESSION['login'])) ? true : false;
}

//دریافت اطلاعات کاربر وارد شده
function getLoggedInUser(){
    return $_SESSION['login'] ?? false;
}

function getCurrentUserId(){
    return getLoggedInUser()->id ?? 0;
}


function logOut(){
    unset($_SESSION['login']);
}

//** REGISTERITION **/
function register($userData){
    global $pdo;
    $sql="INSERT INTO `user` ( `name`, `email`,`password`) VALUES (:name,:email,:pass); ";
    $stmt=$pdo->prepare($sql);
    $pass=password_hash( $userData['password'],PASSWORD_BCRYPT);
    $stmt->execute(array(':name'=>$userData['name'],':email'=>$userData['email'],':pass'=>$pass)  );
    return $stmt->rowCount();
}

