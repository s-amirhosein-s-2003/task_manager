<?php

/***Function Folders***/

//دیافت لیست فولدرها
function getFolders(){
    global $pdo;
    $getCurrentUserId=getCurrentUserId();
    $sql="SELECT * FROM folder WHERE user_id=$getCurrentUserId ";
    $stmt=$pdo->prepare($sql);
    $stmt->execute();
    $records=$stmt->fetchAll(PDO::FETCH_OBJ);

    return $records;
}

//اضافه کردن فولدر
function addFolders($folder_name){
    global $pdo;
    $userId=getCurrentUserId();
    $sql="INSERT INTO `folder` ( `name`, `user_id`) VALUES (:folderName,:userId); ";
    $stmt=$pdo->prepare($sql);
    $stmt->execute(array(':folderName'=>$folder_name,':userId'=>$userId)  );
    return $stmt->rowCount();
}



//حذف فولدر
function deleteFolders($folder_id){
    global $pdo;
    $sql="DELETE FROM `folder` WHERE  id= $folder_id ";
    $stmt=$pdo->prepare($sql);
    $stmt->execute();
    return $stmt->rowCount();
}


/***Function Tasks***/

//دیافت لیست تسکها
function getTasks(){
    global $pdo;
    $getCurrentUserId=getCurrentUserId();
    
    if(isset($_GET['folder_id']) && is_numeric($_GET['folder_id'])){
        $folder_id="AND folder_id={$_GET['folder_id']}";
    }else{
        $folder_id="";
    }
    
    $sql="SELECT * FROM task WHERE user_id=$getCurrentUserId $folder_id";
    $stmt=$pdo->prepare($sql);
    $stmt->execute();
    $records=$stmt->fetchAll(PDO::FETCH_OBJ);
    return $records;
}

//اضافه کردن تسکها
function addTasks($task_name,$folder_id){
    global $pdo;
    $userId=getCurrentUserId();
    $sql="INSERT INTO `task` (`title`, `user_id`, `folder_id`) VALUES (:task_name,:user_id,:folder_id); ";
    $stmt=$pdo->prepare($sql);
    $stmt->execute(array(':task_name'=>$task_name,':user_id'=>$userId,':folder_id'=>$folder_id)  );
    return $stmt->rowCount();
}

//حذف کردن تسکها
function deleteTasks($task_id){
    global $pdo;
    $sql="DELETE FROM `task` WHERE  id= $task_id ";
    $stmt=$pdo->prepare($sql);
    $stmt->execute();
    return $stmt->rowCount();
}

//تغییر وضعیت تسک
function taskSwitch($task_id){
    global $pdo;
    $userId=getCurrentUserId();
    $sql="UPDATE task SET status=1-status WHERE id = :task_id AND user_id = :user_id ; ";
    $stmt=$pdo->prepare($sql);
    $stmt->execute(array(':task_id'=>$task_id,':user_id'=>$userId)  );
    return $stmt->rowCount();

}