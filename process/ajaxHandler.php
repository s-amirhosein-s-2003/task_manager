<?php

include "../bootstrap/init.php";


if(!isAjaxRequest()){
    diepage("Invalid Request !");
}

if(!isset($_POST['action']) || empty($_POST['action']) ){
    diepage("Invalid Action !");
}


switch($_POST['action']){

    case 'addFolder':
        if(empty($_POST['folderName'])){
            echo "لطفا نام پوشه را وارد کنید !";
            die();
        }
        echo addFolders($_POST['folderName']);
    break;

    case 'addTask':
        $task_name=$_POST['taskTitle'];
        $folder_id=$_POST['folderId'];

        if(empty($folder_id) || !isset($folder_id)){
            echo "پوشه مورد نظر را انتخاب کنید ! ";
            die();
        }
        if(empty($task_name) || !isset($task_name)){
            echo "عنوان تسک را وارد کنید ! ";
            die();
        }

        echo addTasks($task_name,$folder_id);
    break;
    case 'doneSwitch':
        $task_id=$_POST['taskId'];
        if(!is_numeric($task_id) || !isset($task_id)){
            echo "آیدی تسک معتبر نیست ";
            die();
        }
        echo taskSwitch($task_id);
    break;
    default:
        diepage("Invalid Action !");
}



?>